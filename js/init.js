---
---


function checkSite(){
    var currentSite = window.location.hostname;
    window.location = "http://" + currentSite;
}

/* ---- particles.js config ---- */

/* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
particlesJS.load('particles-js', '{{ "/assets/particles.json" | prepend:site.baseurl }}', function() {
    console.log('callback - particles.js config loaded');
});

/* parallax effect */
var strength1 = 10;
var strength2 = 20;
var strength3 = 200;
$("html").mousemove(function(e){
    var pageX = e.pageX - ($(window).width() / 2);
    var pageY = e.pageY - ($(window).height() / 2);
    var newvalueX = 1 * pageX * -1;
    var newvalueY = 1 * pageY * -1;
    //$('.on-center .jumbotron h1').css({"padding": (strength1 / $(window).width() * pageX * +1)+"px "+(strength1  / $(window).height() * pageY * +1)+"px"});
    $('#particles-js').find('canvas').css("margin", (strength2 / $(window).width() * pageX * +1)+"px "+(strength2  / $(window).height() * pageY * +1)+"px");
    //$('#foreground').css("background-position", (strength3 / $(window).width() * pageX * -1)+"px "+(strength3  / $(window).height() * pageY * -1)+"px");
});